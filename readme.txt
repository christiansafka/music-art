### Music Art ###

Music Art is a project to create artwork out of music notes.  The idea is to take each note of a song, and assign it a color.  I am currently using:

C - Red
D - Orange
E - Yellow
F - Green
G - Cyan
A - Purple
B - Gray

This remains consistent for sharps and flats so C# would be red.

I started out using music xml notation, but switched to Midi to allow for more variety in songs.  

The octave is also taken into account in my artwork.  The higher the note is on the keyboard, the lighter the color.   

### Progress ###

This application was written in sloppy c#, and you can find a picture of Requiem for a Dream here:   http://i.imgur.com/Efy9TU7.png   (it is darker because a lot of Requiem is played on the lower end of the keyboard).

I will be porting this to a Javascript web app.
